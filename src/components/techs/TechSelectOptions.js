import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getTechs } from '../../actions/techActions';
import Preloader from "../layout/Preloader";

const TechSelectOptions = ({ tech: { techs, loading }, getTechs, setTechName }) => {

  useEffect(() => {
    getTechs();
    //eslint-disable-next-line
  }, []);

  if (loading || techs === null) {
    return <Preloader />;
  }

  return (<select defaultValue="" style={{display: 'block'}} onChange={(e) => setTechName(e.nativeEvent.target[e.target.value].label)}>
            <option value="" disabled>Select a Technician</option>
            {techs.map((tech) => {
              return <option value={tech.id} key={tech.id}>{tech.firstName + " " + tech.lastName}</option>;
            })}
          </select>
  );

};

TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  tech: state.tech,
});

export default connect(mapStateToProps, {getTechs})(TechSelectOptions);
